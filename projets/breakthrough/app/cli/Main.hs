import Game

import Control.Monad
import qualified Data.Vector as V
import Text.Read

main :: IO ()
main = do
    putStrLn "Welcome to Breakthrough!"
    putStrLn "Please enter the first player (R or B):"
    firstPlayer <- readPlayer
    let game = newGame firstPlayer 8
    gameLoop game
    putStrLn "See you next time!"


gameLoop :: Game -> IO()
gameLoop game = do
    putStrLn "Game started!"
    printGame game
    --print here a list of the possible moves
    --read here the move
    putStrLn $ show $ findLegalMoves game 
    putStr "Current player:"
    putStrLn $ case _currentPlayer game of
        PlayerR -> "R"
        PlayerB -> "B"
    putStrLn "Your move ?"
    move <- readMove
    putStrLn "Move done !"
    putStrLn "Next player's turn !"
    
    --update the game
    --check if the game is over
    case gameOver game of
        Just player -> do
            putStrLn "Game over!"
            putStrLn $ case player of
                PlayerR -> "R won!"
                PlayerB -> "B won!"  
        Nothing -> gameLoop $ nextGame $ playMove move game

readPlayer :: IO Player
readPlayer = do
    player <- getLine
    case player of
        "R" -> return PlayerR
        "B" -> return PlayerB
        _ -> do
            putStrLn "Invalid player. Please enter R or B:"
            readPlayer


readMove :: IO Move
readMove = do
    putStrLn "Please enter the move:"
    move <- getLine
    case readMaybe move of
        Just m -> return m
        Nothing -> do
            putStrLn "Invalid move. Please enter a valid move:"
            readMove