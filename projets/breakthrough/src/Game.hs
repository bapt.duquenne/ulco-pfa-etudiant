{-# LANGUAGE StrictData #-}

module Game where

import Control.Monad
import qualified Data.Vector as V

nI, nJ :: Int
nI = 8
nJ = 8

data Player = PlayerR | PlayerB
    deriving (Eq, Show)

data Status = PlayR | PlayB | WinR | WinB
    deriving (Eq, Show)

data Cell = CellE | CellR | CellB
    deriving (Eq, Show)

type Board = V.Vector Cell

type Ix2 = (Int, Int)


type Move = (Ix2, Ix2)

data Game = Game
    { _firstPlayer :: Player
    , _currentPlayer :: Player
    , _status :: Status
    , _board :: Board
    , _moves :: V.Vector Move
    }

ij2k :: Ix2 -> Int
ij2k (i, j) = i*nJ + j

k2ij :: Int -> Ix2
k2ij k = (div k nJ, mod k nJ)

newGame :: Player -> Int -> Game
newGame player size = Game
    { _firstPlayer = player
    , _currentPlayer = player
    , _status = PlayR
    , _board = starterBoard size
    , _moves = V.empty
    }

starterBoard :: Int -> Board
starterBoard size = V.fromList $ replicate size CellB ++ replicate size CellB ++ replicate (size*(size-4)) CellE ++ replicate size CellR ++ replicate size CellR

printGame :: Game -> IO ()
printGame game = do
    forM_ [0..nI-1] $ \i -> do
        forM_ [0..nJ-1] $ \j -> do
            let k = ij2k (i, j)
            putStr $ case _board game V.! k of
                CellE -> "."
                CellR -> "R"
                CellB -> "B"
            putStr " "
        putStrLn ""


isLegalMove :: Game -> Move -> Bool
isLegalMove game move = 
    if _board game V.! (ij2k $ fst move) == CellE then False
    else if snd move == (fst move) then False
    else if _board game V.! (ij2k $ fst move) == CellB && _currentPlayer game == PlayerB && _board game V.! (ij2k $ snd move) /= CellB then 
        if  snd move == (fst(fst move)+1,snd(fst move)+1)  then True
        else if  snd move == (fst(fst move) ,snd(fst move)+1) && _board game V.! (ij2k $ snd move) /= CellE  then True
        else if  snd move == (fst(fst move)-1 ,snd(fst move)+1) then True
        else False
    else if _board game V.! (ij2k $ fst move) == CellR && _currentPlayer game == PlayerR && _board game V.! (ij2k $ snd move) /= CellR then 
        if  snd move == (fst(fst move)+1 ,snd(fst move)-1)  then True
        else if  snd move == (fst(fst move) ,snd(fst move)-1) && _board game V.! (ij2k $ snd move) /= CellE then True
        else if  snd move == (fst(fst move)-1 ,snd(fst move)-1) then True
        else False
    else False


-- verify for every move if it is legal
findLegalMoves :: Game -> V.Vector Move
findLegalMoves game = V.filter (isLegalMove game) (allMoves game)

allMoves :: Game -> V.Vector Move
--match every cell with every other cell
allMoves game = V.fromList [((i,j),(k,l)) | i <- [0..nI-1], j <- [0..nJ-1], k <- [0..nI-1],  l <- [0..nJ-1]]


nextGame :: Game -> Game
nextGame game = game
    { _currentPlayer = if _currentPlayer game == PlayerR then PlayerB else PlayerR
    , _status = if _currentPlayer game == PlayerR then PlayR else PlayB
    }

playMove :: Move -> Game -> Game
playMove move game = game
    { _board = _board game V.// [(ij2k $ fst move, CellE), (ij2k $ snd move, _board game V.! ij2k (fst move))]
    , _moves = _moves game V.++ V.singleton move
    }

gameOver :: Game -> Maybe Player
gameOver game = False
-- vérifier si le joueur a un pion sur la dernière ligne l'opposant
