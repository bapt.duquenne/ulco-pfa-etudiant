{-# LANGUAGE OverloadedStrings , ExtendedDefaultRules #-}
{-# LANGUAGE DeriveGeneric  #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}



import Data.Maybe (fromMaybe)
import System.Environment (lookupEnv)
import Web.Scotty
import View
import Lucid
import Database
import Model
import Control.Monad
import Control.Monad.IO.Class (liftIO)
import Database.Selda (toId)
import Database.Selda.Backend (runSeldaT)
import Database.Selda.SQLite (sqliteOpen, seldaClose)
import Network.Wai.Middleware.Gzip (gzip, def, gzipFiles, GzipFiles(..))
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import System.Directory (doesFileExist)
import Data.Text 
import GHC.Generics

dbFilename :: String
dbFilename = "messages.db"


main :: IO ()
main = do
    port <- read . fromMaybe "3000" <$> lookupEnv "PORT"
    scotty port $ do
        -- dbExists <- doesFileExist dbFilename
        -- conn <- sqliteOpen dbFilename
        -- when (not dbExists) $ runSeldaT dbInit conn

        get "/" $ html $ renderText $ mainPage 
        get "/alldata" $ html $ renderText $ allData testData
        get "/threads/all" $ html $ renderText $ allThreads testThreads
        get "/threads/:id" $ html $ renderText $ "test"