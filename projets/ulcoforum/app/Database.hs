{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}
module Database where
    import Database.Selda
    import Database.Selda.SQLite
    import Model

    instance SqlRow Message

    message_table :: Table Message
    message_table = table "message" [#message_id :- primary , #thread_id :- foreignKey thread_table #id_thread]

        


    instance SqlRow Thread

    thread_table :: Table Thread
    thread_table = table "thread" [#id_thread :- primary]

    dbInit :: SeldaT SQLite IO ()
    dbInit = do
        createTable message_table
        tryInsert message_table
            [ Message 1 1 "user1" "message1"
            , Message 2 1 "user2" "message2"
            , Message 3 2 "user3" "message3"
            ]
            >>= liftIO.print
        createTable thread_table
        tryInsert thread_table
            [ Thread 1 "thread1"
            , Thread 2 "thread2"
            , Thread 3 "thread3"
            ]
            >>= liftIO.print

