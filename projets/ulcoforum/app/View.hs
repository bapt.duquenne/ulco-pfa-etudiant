{-# LANGUAGE OverloadedStrings , ExtendedDefaultRules #-}
{-# LANGUAGE DeriveGeneric  #-}
{-# LANGUAGE DeriveAnyClass #-}

module View where
    import Lucid
    import Model
    import qualified Data.Text.Lazy as L
    import Control.Monad
    import GHC.Generics

    
    mainPage :: Html()
    mainPage = do
        doctypehtml_ $ do
            head_ $ do
                meta_ [charset_ "utf-8"]
            body_ $ do
                h1_ "ulcoforum"
                div_ $ do 
                    a_ [href_ "/alldata"] "alldata"
                    " - "
                    a_ [href_ "/threads/all"] "allthreads"
                "\n this is ulcoforum"

    allData :: [Message] -> Html()
    allData t = do
        doctypehtml_ $ do
            head_ $ do
                meta_ [charset_ "utf-8"]
            body_ $ do
                h1_ "ulcoforum"
                div_ $ do 
                    a_ [href_ "/alldata"] "alldata"
                    " - "
                    a_ [href_ "/threads/all"] "allthreads"
                "\n this is alldata"
                forM_ t $ \m -> do
                    div_ $ do
                        h3_ $ toHtml $ show $ thread_id m
                        "\n *"
                        toHtml $ user m
                        "\n - "
                        toHtml $ message m

    allThreads :: [Thread] -> Html()
    allThreads threads = do
        doctypehtml_ $ do
            head_ $ do
                meta_ [charset_ "utf-8"]
            body_ $ do
                h1_ "ulcoforum"
                div_ $ do 
                    a_ [href_ "/alldata"] "alldata"
                    " - "
                    a_ [href_ "/threads/all"] "allthreads"
                "\n this is all threads"
                forM_ threads $ \t -> do
                    div_ $ do
                        a_ $ toHtml $ thread_name t