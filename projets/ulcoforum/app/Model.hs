{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}


module Model where
    import qualified Data.Text.Lazy as L
    import Data.Aeson
    import GHC.Generics

    data Message = Message
        { message_id :: Int
        , thread_id  :: Int
        , user    :: L.Text
        , message :: L.Text
        } deriving (Show, Generic)

    data Thread = Thread
        { id_thread :: Int
        , thread_name :: L.Text
        } deriving (Show, Generic)

    testData :: [Message]
    testData = 
        [ Message 1 1 "user1" "message1"
        , Message 2 2 "user2" "message2"
        , Message 3 3 "user3" "message3"
        ]

    testThreads :: [Thread]
    testThreads = 
        [ Thread 1 "thread1"
        , Thread 2 "thread2"
        , Thread 3 "thread3"
        ]

    instance ToJSON Message
