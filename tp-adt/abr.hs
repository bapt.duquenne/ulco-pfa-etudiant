import Data.List (foldl')

data Abr a = Feuille | Noeud {
    gauche :: Abr a,
    droite :: Abr a,
    valeur :: a
}deriving(Show)

insererAbr :: Ord a => a -> Abr a -> Abr a
insererAbr ajout Feuille = Noeud Feuille Feuille ajout 
insererAbr va (Noeud g d v) = 
    if va > v then Noeud g (insererAbr v d) va
        else Noeud d (insererAbr v g) va


listToAbr :: [a] -> Abr a
listToAbr list  = func [list] Feuille where
    func [] arb = arb
    func (x:xs) arb = func xs (listToAbr x)

abrToList :: Abr a -> [a]
abrToList Feuille = []
abrToList (Noeud left right value) = (abrToList left) ++ [value] ++ (abrToList right)   

main :: IO ()
main = do
    let a1 = Noeud (12 Feuille Feuille)
    print $ insererAbr 5 (Noeud 13 Feuille Feuille)

