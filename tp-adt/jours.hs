data Jour = Lundi | Mardi | Mercredi | Jeudi | Vendredi | Samedi | Dimanche
    deriving (Show)

estWeekend :: Jour -> Bool
estWeekend Samedi = True 
estWeekend Dimanche = True
estWeekend _ = False

compterOuvrables :: [Jour] -> Int
compterOuvrables tab = length (filter(\x -> not(estWeekend x)) tab)

main :: IO ()
main = do
    putStrLn (show (estWeekend Samedi))
    putStrLn (show (compterOuvrables [Samedi, Dimanche, Lundi]))


