import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Interact
import Numeric

inc :: Float
inc = 0.1

myShowFloat :: Float -> String
myShowFloat v = showFFloat (Just 1) v ""

data Model = Model Float Picture

main :: IO ()
main = do
    bob <- loadBMP "bob.bmp"
    let window = InWindow "hello gloss" (450, 450) (0, 0)
        model = Model 0 bob
        fps = 30
    play window azure fps model hDraw hEvent hTime

hEvent :: Event -> Model -> Model
hEvent _ (Model x b) = Model (x+inc ) b

hTime :: Float -> Model -> Model
hTime _ m = m

hDraw :: Model -> Picture
hDraw (Model x bob) = Scale 0.2 0.2 $ pictures 
    [Text (myShowFloat x)
    , Circle 500
    , Translate (-200) 0 $ Scale 2 2 bob 
    ]