data List a = Nil | Cons a (List a) deriving (Show)


sumList :: List Int -> Int
sumList (Cons x xs)= x+ (sumList (xs))
sumList _ = 0  

flatList :: List String -> String 
flatList (Cons x xs) = x ++ ", "++ (flatList xs)
flatList _ = " "

toHaskell :: List a -> [a]
toHaskell Nil = []
toHaskell (Cons x xs) = x : toHaskell xs

fromHaskell :: [a] -> List a
fromHaskell [] = Nil
fromHaskell (x:xs) = (Cons x (fromHaskell xs)) 

myShowList :: Show a => List a -> String
myShowList (Cons x xs) = (show x) ++ " " ++ myShowList xs 
myShowList (Cons x Nil) = show x
myShowList _ = ""

main :: IO ()
main = do 
    putStrLn (show (sumList(Cons 13 (Cons 42 Nil))))
    putStrLn (show (flatList(Cons "Bon" (Cons "jour !" Nil))))

