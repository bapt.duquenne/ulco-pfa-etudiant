
data User = MkUser
    {
        nom :: String,
        prenom :: String,
        age :: Int
    }

showUser :: User -> String
showUser (MkUser n p a) = n++" "++p++" "++show a++" ans"

incAge :: User -> User
incAge (MkUser n p a) = MkUser n p (a+1)

main :: IO ()
main = do
    putStrLn (showUser (MkUser "John" "Rich" 56))
    putStrLn (showUser (incAge(MkUser "John" "Rich" 56)))
