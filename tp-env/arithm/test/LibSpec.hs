module LibSpec (main, spec) where

import Test.Hspec

import Eval
import Parse

main :: IO ()
main = hspec spec

spec :: Spec
spec = 
    describe "+" $ do
        it "+ 0 2" $ eval (fst $ parse"+ 0 2") `shouldBe` (2::Int)
        it "* 2 2" $ eval (fst $ parse"* 2 2") `shouldBe` (4::Int)

