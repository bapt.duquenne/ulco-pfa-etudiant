import Data.Char

isSorted :: Ord a => [a] -> Bool
isSorted [] = True
isSorted [_] = True
isSorted (x:y:xs)
    | x <= y = isSorted (y:xs)
    | otherwise = False

nocaseCmp :: String -> String -> Bool
nocaseCmp x y = x < ((map toLower) y)

main :: IO ()
main = do
    putStrLn $ show $ isSorted [0..4]
    putStrLn $ show $ isSorted [3,0..4]
    putStrLn $ show $ nocaseCmp "tata" "TOTO"


