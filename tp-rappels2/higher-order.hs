import Data.List

threshList :: Ord a => a -> [a] -> [a]
threshList _ [] = []
threshList maxv list = map (min maxv) list

selectList :: Ord a => a -> [a] -> [a]
selectList maxv tab = select_aux maxv tab []


select_aux :: Ord a => a -> [a] -> [a] -> [a]
select_aux _ [] x = x
select_aux maxv (x:xs) r
    | maxv > x = select_aux maxv xs (r ++ [x])
    | otherwise = select_aux maxv xs r

maxList :: Ord a => [a] -> Maybe a
maxList [] = Nothing
maxList (x:xs) = Just $ foldr max x xs

main :: IO ()
main = do
    putStrLn $ show $ threshList 5 [0..10]
    putStrLn $ show $ selectList 'b' "abcd"
    putStrLn $ show $ maxList "zxspectrum"

