safeTailString :: String -> String
safeTailString "" = ""
safeTailString x = tail x

safeHeadString :: String -> Maybe Char
safeHeadString "" = Nothing
safeHeadString x = Just $ head x


safeTail :: [x] -> [x]
safeTail [] = []
safeTail x = tail x

 
safeHead :: [x] -> Maybe x
safeHead x = Just $ head x


main :: IO ()
main = do
    putStrLn $ show $ safeHeadString "test"
    putStrLn $ show $ safeTailString "test"
    putStrLn $ show $ safeHead [0,1,2,3]
    putStrLn $ show $ safeTail [0,1,2,3]

