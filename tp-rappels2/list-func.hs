
mymap1 :: (a -> b) -> [a] -> [b] 
mymap1 _ [] = []
mymap1 func (x:xs) = [func x] ++ mymap1 func xs 

mymap2 :: (a -> b) -> [a] -> [b] 
mymap2 _ [] = []
mymap2 func tab = aux func tab [] where
    aux _ [] res = res
    aux func (x:xs) res = aux func xs ([func x]++res)

myfilter1 :: (a -> Bool) -> [a] -> [a]
myfilter1 _ [] = [] 
myfilter1 func (x:xs) = if func x then myfilter1 func xs ++ [x] else myfilter1 func xs

myfilter2 :: (a -> Bool) -> [a] -> [a]
myfilter2 _ [] = [] 
myfilter2 func tab = aux func tab [] where
    aux _ [] res = res
    aux func (x:xs) res 
        | func x = aux func xs [x]++res
        | otherwise = aux func xs res


-- myfoldl :: Foldable t => (b -> a -> b) -> b -> t a -> b
-- jsp :(

-- myfoldr :: Foldable t => (b -> a -> b) -> b -> t a -> b
-- jsp :(

main :: IO ()
main = do
    putStrLn $ show $ mymap1 (\x -> x*2) [0..2]
    putStrLn $ show $ mymap2 (\x -> x*2) [0..2]
    putStrLn $ show $ myfilter1 (\x -> x < 5) [0..10]
    putStrLn $ show $ myfilter2 (\x -> x < 5) [0..10]


    --test 


