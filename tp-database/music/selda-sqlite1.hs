{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

import Database.Selda
import Database.Selda.SQLite

data Title = Title
    {   title_name :: Text,
        title_id :: ID Title,
        title_artist :: Int
    }deriving(Generic , Show)

instance SqlRow Title


data Artist = Artist
    {   artist_id :: ID Artist,
        artist_name :: Text
    }deriving(Generic , Show)

instance SqlRow Artist

artist_table :: Table Artist
artist_table = table "artist" [#artist_id :-autoPrimary]

title_table :: Table Title
title_table = table "title" [#title_id :-autoPrimary
                            , #title_artist :- foreignKey artist_table #artist_id ]



selectAllTitles :: SeldaT SQLite IO [Title]
selectAllTitles = query $ do
    a <- select artist_table
    b <- select title_table
    restrict (a ! #artist_id .== t ! #title_artist)
    return (a ! #artist_name :*: t ! #title_name)


main :: IO()
main = withSQLite "music.db" selectAllTitles >>= mapM_ print