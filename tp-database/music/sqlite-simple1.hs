{-# LANGUAGE OverloadedStrings #-}

import Data.Text
import Database.SQLite.Simple

selectAllMusic:: Connection -> IO [(Text,Text)]
selectAllMusic conn = query_ conn "SELECT artist_name, title_name \
                            \FROM title \
                            \INNER JOIN artist ON artist_id = title_artist"

data Artist = Artist
    {   artist_id :: Int,
        artist_name :: Text
    }deriving(Show)

instance FromRow Artist where
    fromRow = Artist <$> field <*> field

selectAllADT:: Connection -> IO[Artist]
selectAllADT conn= query_ conn "SELECT * FROM artist"

main::IO()
main = do
    withConnection "music.db" selectAllMusic >>= mapM_ print
    withConnection "music.db" selectAllADT >>= mapM_ print

