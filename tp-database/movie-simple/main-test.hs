import Database.SQLite.Simple (open, close, withConnection)

import Movie1
import Movie2

main :: IO ()
main = do
    conn <- open "movie.db"

    putStrLn "\nMovie2.dbSelectAllMovies"
    withConnection "movie.db" dbSelectAllMovies >>= mapM_ print

    putStrLn "\nMovie2.dbSelectAllProds"
    withConnection "movie.db" dbSelectAllProds >>= mapM_ print

    close conn

