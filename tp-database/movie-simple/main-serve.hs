{-# LANGUAGE OverloadedStrings , ExtendedDefaultRules #-}
{-# LANGUAGE DeriveGeneric  #-}
{-# LANGUAGE DeriveAnyClass #-}

import Lucid
import Web.Scotty
import qualified Data.Text.Lazy as L
import Network.Wai.Middleware.Gzip (gzip, def, gzipFiles, GzipFiles(..))
import Database.SQLite.Simple (open, close, withConnection)
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import qualified Data.Aeson as DA
import GHC.Generics
import Movie2
import Control.Monad
import Control.Monad.IO.Class (liftIO)

htmlPage:: [ProdInfo] ->  Html()
htmlPage prods = do
    doctypehtml_ $ do
            head_ $ do
                meta_ [charset_ "utf-8"]
            body_ $ do
                ul_ $ do
                    forM_ prods $ \prod -> do
                        li_ $ do
                            b_ (toHtml $ _movie prod)
                            " "
                            span_ (toHtml $ show $ _year prod)
                            " "
                            span_ (toHtml $ _person prod)
                            " "
                            span_ (toHtml $ _role prod)

                    

main :: IO ()
main = scotty 3000 $ do
    middleware logStdoutDev
    middleware $ gzip def { gzipFiles = GzipCompress }
    get "/" $ do
        prods <- liftIO $ withConnection "movie.db" dbSelectAllProds 
        html $ renderText $  htmlPage $ prods