{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Movie2 where

    import Data.Text (Text)
    import Database.SQLite.Simple
    import Database.SQLite.Simple.FromRow (FromRow, fromRow, field)
    import GHC.Generics (Generic)

    data Movie = Movie  
        {
            movie_id :: Int,
            movie_title :: Text,
            movie_year :: Int
        } deriving(Show)

    instance FromRow Movie where
        fromRow = Movie <$> field <*> field <*> field

    dbSelectAllMovies:: Connection -> IO[Movie]
    dbSelectAllMovies conn = query_ conn "Select * from movie"


    data ProdInfo = ProdInfo
        {   _movie :: Text,
            _year :: Int,
            _person :: Text,
            _role :: Text
        } deriving(Show)

    instance FromRow ProdInfo where
        fromRow = ProdInfo <$> field <*> field <*> field <*> field

    dbSelectAllProds:: Connection -> IO[ProdInfo]
    dbSelectAllProds conn = query_ conn "SELECT movie_title, movie_year, person_name,role_name FROM movie\
                                        \ INNER JOIN prod ON prod_movie = movie_id \
                                        \ INNER JOIN person ON prod_person = person_id \
                                        \ INNER JOIN role ON prod_role = role_id"

    