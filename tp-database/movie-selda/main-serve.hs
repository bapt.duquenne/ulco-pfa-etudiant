{-# LANGUAGE OverloadedStrings , ExtendedDefaultRules #-}
{-# LANGUAGE DeriveGeneric  #-}
{-# LANGUAGE DeriveAnyClass #-}

import Lucid
import Web.Scotty
import qualified Data.Text.Lazy as L
import Network.Wai.Middleware.Gzip (gzip, def, gzipFiles, GzipFiles(..))
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import qualified Data.Aeson as DA
import GHC.Generics
import Movie
import Control.Monad
import Control.Monad.IO.Class (liftIO)
import Database.Selda (toId)
import Database.Selda.Backend (runSeldaT)
import Database.Selda.SQLite (sqliteOpen, seldaClose)
import System.Directory (doesFileExist)

dbFilename :: String
dbFilename = "movie.db"

htmlPage:: [Movie :*: Role :*: Person] ->  Html()
htmlPage prods = do
    doctypehtml_ $ do
            head_ $ do
                meta_ [charset_ "utf-8"]
            body_ $ do
                ul_ $ do
                    forM_ prods $ \(m :*: p :*: r)) -> do
                        li_ $ do
                            b_ (toHtml $ movie_name m)
                            " "
                            span_ (toHtml $ show $ movie_year m)
                            " "
                            span_ (toHtml $ person_name p)
                            " "
                            span_ (toHtml $ role_name r)

                    

main :: IO ()
main = scotty 3000 $ do
    dbExists <- doesFileExist dbFilename
    conn <- sqliteOpen dbFilename
    when (not dbExists) $ runSeldaT dbInit conn
    middleware logStdoutDev
    middleware $ gzip def { gzipFiles = GzipCompress }
    get "/" $ do
        prods <- liftIO $ runSeldaT dbSelectAllProds conn >>= mapM_ print
        html $ renderText $  htmlPage $ prods