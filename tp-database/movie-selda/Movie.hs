{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

module Movie where

  import Control.Monad.IO.Class (liftIO)
  import Database.Selda
  import Database.Selda.SQLite

  ----------------------------------------------------------------------
  -- Movie
  ----------------------------------------------------------------------

  data Movie = Movie
    { movie_id :: ID Movie
    , movie_title :: Text
    , movie_year :: Int
    } deriving (Generic, Show)

  instance SqlRow Movie

  movie_table :: Table Movie
  movie_table = table "movie" [#movie_id :- autoPrimary]

  ----------------------------------------------------------------------
  -- Person
  ----------------------------------------------------------------------

  data Person = Person
    { person_id :: ID Person
    , person_name :: Text
    } deriving (Generic, Show)

  instance SqlRow Person

  person_table :: Table Person
  person_table = table "person" [#person_id :- autoPrimary]


  ----------------------------------------------------------------------
  -- Role
  ----------------------------------------------------------------------

  data Role = Role
    { role_id :: ID Role
    , role_name :: Text
    } deriving (Generic, Show)
    
  instance SqlRow Role

  role_table :: Table Role
  role_table = table "role" [#role_id :- autoPrimary]

  ----------------------------------------------------------------------
  -- Prod
  ----------------------------------------------------------------------

  data Prod = Prod
    { prod_movie :: ID Movie
    , prod_person :: ID Person
    , prod_role :: ID Role
    } deriving (Generic, Show)
    
  instance SqlRow Prod

  prod_table :: Table Prod
  prod_table = table "role" [#prod_role :- foreignKey role_table #role_id,
                             #prod_person :- foreignKey person_table #person_id,
                             #prod_movie :- foreignKey movie_table #movie_id]


  -----------------------------
  -- ProdInfo
  -----------------------------

  data ProdInfo = ProdInfo
      {   _movie :: Text,
          _year :: Int,
          _person :: Text,
          _role :: Text
      } deriving(Show)

  ----------------------------------------------------------------------
  -- queries
  ----------------------------------------------------------------------

  dbInit :: SeldaT SQLite IO ()
  dbInit = do

      createTable movie_table
      tryInsert movie_table
          [ Movie def "Bernie" 1996
          , Movie def "Le Kid" 1921 
          , Movie def "Metropolis" 1927
          , Movie def "Citizen Kane" 1941 ]
          >>= liftIO.print

  dbSelectAllMovies:: SeldaT SQLite IO [Movie]
  dbSelectAllMovies = query $ select movie_table

  
  dbSelectAllProds::SeldaT SQLite IO [Movie :*: Role :*: Person]
  dbSelectAllProds = query $ do
    prods <- select prod_table
    movie <- select movie_table
    role <- select role_table
    person <- select person_table
    restrict ( prods ! #prod_movie .== movie ! #movie_id)
    restrict ( prods ! #prod_role .== role ! #role_id)
    restrict ( prods ! #prod_person .== person ! #person_id)
    return (movie :*: role :*: person)

  dbSelectMoviesFromPersonId:: ID Person -> SeldaT SQLite IO [Movie]
  dbSelectMoviesFromPersonId id = query $ do
    movies <- select movie_table
    prods <- select prod_table
    restrict ( prods ! #prod_movie .== movies ! #movie_id)
    restrict ( prods ! #prod_person .== (literal id))
    return movies


    



