import Data.Char (toUpper)
import Data.List.Split (splitOn)

data User = User
    { _name :: String
    , _email :: String
    } deriving Show

parseUser :: String -> Maybe User
parseUser str= case splitOn ";" str of
    [n, e] -> Just $ User n e 
    _ -> Nothing

upperize :: User -> User
upperize (User n e) = User (map toUpper n) e 

--main :: IO ()
--main = do
--    putStrLn "enter name and email separed by a semi colon."
--    str <- getLine
--    case parseUser str of
--        Nothing -> putStrLn "nothing !"
--        Just u -> putStrLn $ show $ upperize u


main :: IO ()
main = getLine >>= print . (fmap upperize . parseUser)
