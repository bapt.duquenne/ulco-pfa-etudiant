type MyNum = Either String Double

mySqrt :: Double -> MyNum
mySqrt n
    | n < 0 = Left "error : sqrt number is inferior to 0"
    | otherwise = Right (sqrt n)

myLog :: Double -> MyNum
myLog n
    | n <= 0 = Left "error : log number is inferior or equal to 0"
    | otherwise = Right (log n)

myMul2 :: Double -> MyNum
myMul2 n = Right (n*2)

myNeg :: Double -> MyNum
myNeg n = Right (n*(-1))

myCompute1 :: MyNum
myCompute1 = 
    case mySqrt 16 of
        Left e1 -> Left e1
        Right e2 -> case myNeg e2 of
            Left e1 -> Left e1
            Right e2 -> myMul2 e2

myCompute2 :: MyNum
myCompute2 = do
    a <- mySqrt 16
    b <- myNeg a
    c <- myLog b
    return c

myCompute3 :: MyNum
myCompute3 = myNeg 16 >>= mySqrt >>= myNeg >>= myLog

main :: IO ()
main = do
    print myCompute1
    print myCompute2
    print myCompute3

