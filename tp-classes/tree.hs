data Tree a = Leaf | Node a (Tree a) (Tree a)

instance Show a => Show (Tree a) where
    show (Node v l r) = "(" ++ show v ++ show l ++ show r ++ ")"
    show Leaf = "_"

instance Foldable Tree where
    foldMap f Leaf = mempty
    foldMap f (Node x l r) = foldMap f l `mappend` f x `mappend` foldMap f r


mytree1 :: Tree Int
mytree1 = Node 7 (Node 2 Leaf Leaf)
                (Node 37 (Node 13 Leaf Leaf)
                         (Node 42 Leaf Leaf))
mytree2 :: Tree Double
mytree2 = Node 7.0 (Node 2.0 Leaf Leaf)
                (Node 37.0 (Node 13.0 Leaf Leaf)
                         (Node 42.0 Leaf Leaf))

main = do
    print mytree1
    print mytree2
    print $ sum mytree1
    print $ maximum mytree1

