{-# LANGUAGE OverloadedStrings #-}

import Music

import Control.Monad (forM_)
import Data.Text.Lazy (pack)
import Lucid
import Web.Scotty

newtype Model = Model { _musics :: [Music] }

main :: IO ()
main = do
    let port = 3000
        model = Model myMusics
    scotty port (serverApp model)


serverApp :: Model -> ScottyM ()
serverApp _model = do

    get "/api" $ json $ myMusics

    get "/api/artist/:name" $ do
        name <- param "name"
        json $ findFromBand name

    get "/" $ html $ renderText  $ doctypehtml_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            meta_ [ name_ "viewport"
                  , content_ "width=device-width,initial-scale=1,shrink-to-fit=no"]
            title_ "Music"

        body_ $ do
            h1_ "Music"
            h2_ "All Music"
            ul_ $
            --forM_ myMusics $ \music -> do
            --    li_ ( toHtml $ title music )
            -- TODO


