{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}

import Network.Wai.Handler.Warp (run)
import Servant 

import MathApi

type ServerApi
    =    Math42 
    :<|> MathMul2
    :<|> MathAdd

handleServerApi :: Server ServerApi
handleServerApi
    =    (return 42)
    :<|> handleMathMul2
    :<|> handleAdd

handleMathMul2 :: Int -> Handler Int
handleMathMul2 x
    =   (return (x*2))

handleAdd :: Int -> Int -> Handler Int
handleAdd x y =
    (return (x + y))

serverApp :: Application
serverApp = serve (Proxy @ServerApi) handleServerApi

main :: IO ()
main = do
    putStrLn "listening..."
    run 3000 serverApp

