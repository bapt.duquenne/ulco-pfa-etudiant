{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module MathApi where

import Servant 

type Math42 = "quarante-deux" :> Get '[JSON] Int

type MathMul2 = "multiplie_par_deux":> Capture "number" Int:> Get '[JSON] Int

type MathAdd =Capture "number1" Int:> "plus":> Capture "number2" Int:> Get '[JSON] Int

