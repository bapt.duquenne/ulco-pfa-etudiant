import Options.Applicative

data Args = Args
    { output     :: String
    , hello      :: String
    , val1       :: Int
    , val2       :: Double
    } deriving (Show)

fileP :: Parser String
fileP =  strArgument
          ( help "file input"
         <> metavar "IN_PATH")


argsP :: Parser Args
argsP = Args
      <$> strArgument
          ( help "file output"
         <> metavar "OUT_PATH")
      <*> strOption
          ( long "hello"
         <> help "Target for the greeting"
         <> metavar "TARGET")
      <*> option auto
          ( long "val1"
         <> help "Value 1"
         <> metavar "INT"
         <> value 1)
      <*> option auto
          ( long "val2"
         <> help "Value 2"
         <> metavar "Double"
         <> value 2)

superParser :: Parser (String, Args) 
superParser = (,) <$> fileP <*> argsP

myinfo :: ParserInfo (String, Args)
myinfo = info (superParser <**> helper)
              (fullDesc <> header "This is my cool app!")

main :: IO ()
main = execParser myinfo >>= print

