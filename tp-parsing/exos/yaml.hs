{-# LANGUAGE DeriveGeneric #-}

import Data.Yaml
import GHC.Generics
import qualified Data.Text as T

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    } deriving (Show, Generic)

instance FromJSON Person

main :: IO()
main = do
    person <- decodeFileEither "yaml-test1.yaml" :: IO(Either ParseException Person)
    print person