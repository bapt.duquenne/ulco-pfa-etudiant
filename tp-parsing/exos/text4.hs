import qualified Data.ByteString.Char8 as B
import qualified Data.Text.Encoding as TE
import qualified Data.Text.IO as TI

main::IO()
main = do
    file <- TI.readFile "text1.hs"
    let str = TE.encodeUtf8 file
    B.putStrLn str