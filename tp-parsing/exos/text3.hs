import qualified Data.ByteString.Char8 as B
import qualified Data.Text.Encoding as TE
import qualified Data.Text.IO as TI

main::IO()
main = do
    file <- B.readFile "text1.hs"
    let str = TE.decodeUtf8 file
    TI.putStrLn str