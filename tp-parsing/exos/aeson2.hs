{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import qualified Data.Text as T
import Data.Aeson

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: T.Text
    , speakenglish :: Bool
    } deriving (Show)

instance FromJSON Person where
    parseJSON = withObject "Person" $ \v -> Person 
        <$> v .: "firstname"
        <*> v .: "lastname"
        <*> v .: "birthyear"
        <*> v .: "speakenglish"


main :: IO ()
main = do
    jsonfile <- eitherDecodeFileStrict "aeson-test1.json" ::IO(Either String Person)
    print jsonfile

    jsonfile2 <- eitherDecodeFileStrict "aeson-test2.json" ::IO(Either String [Person])
    print jsonfile2

    jsonfile3 <- eitherDecodeFileStrict "aeson-test3.json" ::IO(Either String [Person])
    print jsonfile3

