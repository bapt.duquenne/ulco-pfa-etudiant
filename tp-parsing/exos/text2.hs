import qualified Data.Text as T
import qualified Data.Text.IO as TI

main::IO()
main = do
    file <- TI.readFile "text1.hs"
    let re = T.unpack file
    putStrLn re