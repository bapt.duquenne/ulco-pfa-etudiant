import qualified Data.Text.IO as TI
import qualified Data.Text.Lazy.IO as TIL
import qualified Data.Text.Lazy as TL

main::IO()
main = do
    file <- TI.readFile "text1.hs"
    let re = TL.fromStrict file
    TIL.putStrLn re