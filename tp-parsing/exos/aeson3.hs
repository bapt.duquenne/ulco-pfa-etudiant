{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import Data.Aeson

data Person = Person
    { first    :: T.Text
    , last     :: T.Text
    , birth    :: Int
    } deriving (Show)

instance FromJSON Person where
    parseJSON = withObject "Person" $ \v -> do
        f <- v .: "firstname"
        l <- v .: "lastname"
        b <- v .: "birthyear"
        return (Person f l (read b))


main :: IO ()
main = do
    jsonfile <- eitherDecodeFileStrict "aeson-test1.json" ::IO(Either String Person)
    print jsonfile

    jsonfile2 <- eitherDecodeFileStrict "aeson-test2.json" ::IO(Either String [Person])
    print jsonfile2

    jsonfile3 <- eitherDecodeFileStrict "aeson-test3.json" ::IO(Either String [Person])
    print jsonfile3


