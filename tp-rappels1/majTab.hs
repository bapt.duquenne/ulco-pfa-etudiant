import Data.Array
import Text.Read (readMaybe)

type Tab = Array Int Double

newTab :: Tab
newTab = array (1,4) [(1,0.0),(2,0.0),(3,0.0),(4,0.0)]

loopMaj :: Tab -> IO Tab
loopMaj t = do
    print $ t
    x <-getLine
    case readMaybe x :: Maybe (Int, Double) of
        Nothing -> loopMaj t
        Just x -> loopMaj (t // [x])


    

main :: IO ()
main = do
    print newTab 

