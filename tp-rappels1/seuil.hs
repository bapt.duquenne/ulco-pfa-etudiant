seuilInt :: Int -> Int -> Int -> Int
seuilInt min max value
    | value < min = min
    | value > max = max
    | otherwise = value 

seuilTuple :: (Int , Int) -> Int -> Int
seuilTuple borne value
    | value < (fst borne) = (fst borne)
    | value > (snd borne) = (snd borne)
    | otherwise = value 


main :: IO ()
main = do
    print $ seuilTuple (1, 10) 0
    print $ seuilInt 1 10 0
