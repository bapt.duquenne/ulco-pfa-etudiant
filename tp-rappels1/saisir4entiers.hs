import Control.Monad (forM_)
import System.IO (hFlush, stdout)
import Text.Read (readMaybe)

test :: String -> String
test x = 
    case readMaybe x :: Maybe Int of
        Nothing -> "saisie invalide"
        _ -> "vous avez saisi l'entier "++ (show x)

boucle :: Int -> IO()
boucle 5 = return ()
boucle i = do 
    putStr ("saisie "++(show i)++" : ")
    x <- getLine
    putStrLn $ test x
    boucle (i+1)

main :: IO ()
main = do
    boucle 1





