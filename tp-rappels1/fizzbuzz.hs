
fizzbuzz1 :: [Int] -> [String]
fizzbuzz1 [] = []
fizzbuzz1 (x :xs) 
    | (x `mod` 3)== 0 = "fizz": (fizzbuzz1 xs)
    | (x `mod` 5)== 0 = "buzz": (fizzbuzz1 xs)
    | (x `mod` 5)== 0 && (x `mod` 3)== 0 = ("fizzbuzz": fizzbuzz1 xs)
    | otherwise = ((show x) : fizzbuzz1 xs)


go :: [Int] -> [String] -> [String]
go [] acc= acc
go (x :xs) acc
    | (x `mod` 3)== 0 = (go xs (acc ++ ["fizz"]))
    | (x `mod` 5)== 0 = (go xs (acc ++ ["buzz"]))
    | (x `mod` 5)== 0 && (x `mod` 3)== 0 = (go xs (acc ++ ["fizzbuzz"]))
    | otherwise = go xs (acc ++ [show x])

fizzbuzz2 :: [Int] -> [String]
fizzbuzz2 t = go t []

main :: IO()
main = putStrLn "TODO"

