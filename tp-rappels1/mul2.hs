mul2 :: Int -> Int
mul2 x = 2*x 

main :: IO ()
main = do
    putStrLn (show (mul2 42))
    putStrLn (show (mul2 (-42)))

