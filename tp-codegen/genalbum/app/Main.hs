{-# LANGUAGE OverloadedStrings , ExtendedDefaultRules #-}
{-# LANGUAGE DeriveGeneric #-}

import Lucid
import Data.Aeson
import qualified Data.Text.Lazy as T
import GHC.Generics
import Control.Monad

data Album = Album
    {
        imgs :: [T.Text]
        ,url :: T.Text
    }deriving(Show, Generic)

instance FromJSON Album


generateMainHtml :: [Album] -> Html()
generateMainHtml albums = do
    doctypehtml_ $ do
            head_ $ do
                meta_ [charset_ "utf-8"]
            body_ $ do
                forM_ albums $ \x -> do 
                    h2_ $ toHtml $ url x
                    forM_ (imgs x) $ \y -> do
                        let imgSrc = if "http" `T.isPrefixOf` y
                            then y
                            else url x <> "/" <> y
                        img_ [src_ (T.toStrict imgSrc)]

generateSlideHtml:: Int -> T.Text -> T.Text -> T.Text-> Html()
generateSlideHtml x previous next img = 
    doctypehtml_ $ do
            head_ $ do
                meta_ [charset_ "utf-8"]
            body_ $ do
                h1_ $ do
                    "Slide"
                    toHtml $ show x
                p_ $ do
                    a_ [href_ previous] "previous"
                    " - "
                    a_ [href_ next] "next"
                img_ [src_ img]

                        

generateSlides:: [Album] -> IO()
generateSlides albums = do
    
                    


main :: IO ()
main = do
    jsonfile <- eitherDecodeFileStrict' "../../data/genalbum.json" 
    case jsonfile of
        Left err -> putStrLn err
        Right val -> do
            renderToFile "myPage.html" (generateMainHtml val)

