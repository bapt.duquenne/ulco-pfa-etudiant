{-# LANGUAGE OverloadedStrings , ExtendedDefaultRules #-}

import Lucid

x :: Int
x = 42

myPage :: Html()
myPage =do
    doctypehtml_ $ do
            head_ $ do
                meta_ [charset_ "utf-8"]
            body_ $ do
                toHtml $ show x
                h1_ "hello"
                img_ [src_ "toto.png"]  
                p_ $ do 
                    "this is "
                    a_ [href_ "toto.png"] $ "a link"
                


main::IO()
main = renderToFile "myPage.html" myPage