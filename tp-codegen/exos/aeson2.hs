{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric  #-}
{-# LANGUAGE DeriveAnyClass #-}


import qualified Data.Text as T
import Data.Aeson

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    } deriving (Show)

instance ToJSON Person where
    toJSON(Person _firstname _lastname _birthyear) =
        object [ "first" .= _firstname
                ,"last" .= _lastname
                ,"birth" .= _birthyear]

persons :: [Person]
persons =
    [ Person "John" "Doe" 1970
    , Person "Haskell" "Curry" 1900
    ]

main :: IO()
main = encodeFile "out-aeson2.json" persons
    
