{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric  #-}
{-# LANGUAGE DeriveAnyClass #-}


import qualified Data.Text as T
import Data.Aeson
import GHC.Generics

data Address = Address
    {   road :: T.Text,
        zipcode :: Int,
        city :: T.Text,
        number :: Int
    }deriving(Generic, Show)
   
instance ToJSON Address  

data Person = Person
    { firstname    :: T.Text
    , address      :: Address
    , lastname     :: T.Text
    , birthyear    :: Int
    } deriving (Show, Generic)

instance ToJSON Person 

persons :: [Person]
persons =
    [ Person "John" (Address "test" 11000 "test" 34)"Doe" 1970
    , Person "Haskell" (Address "test" 11000 "test" 35) "Curry" 1900
    ]

main :: IO()
main = encodeFile "out-aeson3.json" persons
    
