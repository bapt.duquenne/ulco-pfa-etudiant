{-# LANGUAGE OverloadedStrings , ExtendedDefaultRules #-}

import Lucid
import qualified Data.Text.Lazy.IO as T

myPage :: Html()
myPage =do
    h1_  $ div_ "hello world !"

main::IO()
main = T.putStrLn $ renderText myPage