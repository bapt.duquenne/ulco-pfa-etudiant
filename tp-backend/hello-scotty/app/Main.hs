{-# LANGUAGE OverloadedStrings , ExtendedDefaultRules #-}
{-# LANGUAGE DeriveGeneric  #-}
{-# LANGUAGE DeriveAnyClass #-}

import Lucid
import Web.Scotty
import qualified Data.Text.Lazy as L
import Network.Wai.Middleware.Gzip (gzip, def, gzipFiles, GzipFiles(..))
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.Static (addBase, staticPolicy)
import qualified Data.Aeson as DA
import GHC.Generics

data Person = Person
    { _name :: L.Text
    , _year :: Int
    }deriving (Show, Generic)

instance DA.ToJSON Person

data TextObject = TextObject L.Text deriving (Show, Generic)
instance DA.ToJSON TextObject

persons :: [Person]
persons =
    [ Person "John"  1970
    , Person "Haskell" 1900
    ]

testPage :: Html()
testPage = do
    doctypehtml_ $ do
            head_ $ do
                meta_ [charset_ "utf-8"]
            body_ $ do
                h1_ "hello"

myPage :: Html()
myPage =do
    doctypehtml_ $ do
            head_ $ do
                meta_ [charset_ "utf-8"]
            body_ $ do
                h1_ "hello-scotty"
                p_ $ do 
                    ul_ $ do
                        li_ $
                            a_ [href_ "/route1"] "route 1"
                        li_ $
                                a_ [href_ "/route1/route2"] "/route1/route2"
                        li_ $
                                a_ [href_ "/html1"] "html1"
                        li_ $
                                a_ [href_ "/json1"] "json1"
                        li_ $
                                a_ [href_ "/json2"] "json2"
                        li_ $
                                a_ [href_ "/add1/2/2"] "add"
                img_ [src_ "bob.png"]  

main :: IO ()
main = scotty 3000 $ do
    middleware logStdoutDev
    middleware $ gzip def { gzipFiles = GzipCompress }
    middleware $ staticPolicy $ addBase "static"
    get "/" $ html $ renderText myPage
    get "/route1" $ text "hello "
    get "/route1/route2" $ text "world !" 
    get "/html1" $ html $ renderText testPage
    get "/json1" $ json $ TextObject "oui"
    get "/json2" $ json persons
    get (capture "/add1/:a/:b") $ do
        xStr <- param "a"
        yStr <- param "b"
        let x = read xStr :: Double
        let y = read yStr :: Double
        json (x+y)
    get (capture "/add2") $ text "null"
