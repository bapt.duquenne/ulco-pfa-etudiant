{-# LANGUAGE OverloadedStrings , ExtendedDefaultRules #-}
{-# LANGUAGE DeriveGeneric  #-}
{-# LANGUAGE DeriveAnyClass #-}

module View where
    import Lucid
    import Model
    import qualified Data.Text.Lazy as L
    import Control.Monad
    
    mainPage ::[Rider]-> Html()
    mainPage riders = do
        doctypehtml_ $ do
                head_ $ do
                    meta_ [charset_ "utf-8"]
                body_ $ do
                    forM_ riders $ \rider -> do
                        h2_ (toHtml $ name rider)
                       -- forM_ (picture_link rider) $ \picture -> do
                       --     img_ [src_ (T.toStrict picture)]
                    

