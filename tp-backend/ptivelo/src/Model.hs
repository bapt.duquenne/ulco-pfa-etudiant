{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric  #-}
{-# LANGUAGE DeriveAnyClass #-}

module Model where
    import qualified Data.Text.Lazy as L
    import Data.Aeson
    import GHC.Generics

    data Rider = Rider
        { name    :: L.Text
        , picture_link :: [L.Text]
        } deriving (Show, Generic)

    instance ToJSON Rider


    
